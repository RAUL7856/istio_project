#Working with Istio 

##Installation:  
download [istio]
(curl -L https://istio.io/downloadIstio) add bin to path.

```bash
mkdir ~/bin
```
```bash
cp ./bin/istioctl ~/bin
```

```bash
cat << EOF >> ~/.bashrc
export PATH="~/bin:\$PATH"
EOF
```
install istioctl

```bash
istioctl install 
```
Check for istio-system name space
```bash
Kubectl get ns
```
```bash
Kubectl get pod –n istio-system
```

##Sidecar Injection
Manual

Automatic
```bash
kubectl label namespace default istio-injecton=enabled

#Verify that label has been applied
Kubectl get ns –Listio-injections
```

##Application deployment
```bash
kubectl apply -f backend.yaml

kubectl apply -f frontend.yaml

#Sleep helps run curl in services 
kubectl apply -f samples/sleep/sleep.yaml
```
##Check
```bash
#Check if acess services are working 
kubectl exec deploy/sleep -- curl -s frontend | head

kubectl exec deploy/sleep -- curl -s backend
```

##Ingress
```bash
#Check Ingress gateway pod
kubectl get pod -n istio-system

#Check Loadbalancer
kubectl get svc -n istio-system

#Get the external ip for load balancer
export GATEWAY_IP=$(kubectl get svc -n istio-system istio-ingressgateway \-ojsonpath='{.status.loadBalancer.ingress[0].ip}')
```
Associating Ip address with a hostname via DNS is usual way
but here i am acessing it without dns

##gateway.yml
```bash
kubectl apply -f gateway.yaml
attempt HTTP request 
curl -v http://$GATEWAY_IP/
```
##virtual service
```bash
kubectl apply -f frontend-virtualservice.yaml
kubectl get virtualservice
```

##Ingress with TLS
Generate a self-signed root certificate
Generate a certificate and a private key for the host name
Store the certificates as secrets in kubernetes
Revise the gateway and apply

##Observability
```bash
kubectl apply -f samples/addons/prometheus.yaml
#Verify istio-system namespace is running additional loads 
kubectl get pod -n istio-system

#Generating load for test
while true; do curl -I http://$GATEWAY_IP; sleep 0.5; done

istioctl dashboard prometheus
```

##Mutual Tls
```bash
#set Peerauthentication mode to strict

kubectl apply -f mtls-strict.yaml

#verify 
kubectl get peerauthentication
```
##authorization policy 
we can use this policy to restrict service to call one more service
Nomanclature of principal comes from spiffe standard
```bash
kubectl apply -f authorization-policy-backend.yaml 

#Get service  names
kubectl get pod -n istio-system -l app=istio-ingressgateway -o yaml | grep serviceAccountName
```

##Deploy a new version
```bash
#Check app service label
kubectl get pod -Lapp,version

#Selector on services
kubectl get svc backend -o wide

#Create destination rule with different subsets
Kubectl apply –f backend-destinationrule.yml

#Verify
kubectl get destinationrule

istioctl x describe svc backend

#Allow only v1 subnet
Backend-virtualservice.yaml

#Deploy backend-v2.yaml
Kubectl apply –f backend-v2.yaml

#generate some traffic
while true; do curl -I http://$GATEWAY_IP/; sleep 0.5; done

#Exposing debug traffic to v2
Kubectl apply –f Customer-vs-debug.yaml

#Test on a web browser

#Split the traffic using weight just to be cautious 
Kubectl apply –f backend-vs-canary.yaml
```





























